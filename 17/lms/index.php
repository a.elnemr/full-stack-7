<?php

if (!isset($_GET['page']) || empty($_GET['page'])) {
    $page = 'home';
} else {
    $page = $_GET['page'];
}

switch ($page) {
    case 'home':
        require_once 'view/home.php';
        break;
    case 'users':
        require_once 'view/users.php';
        break;
    default:
        require_once 'view/404.php';
}