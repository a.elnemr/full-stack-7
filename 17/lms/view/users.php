<?php
require_once '../config/dbConfig.php';
require_once '../model/User.php';

if (isset($_GET['action'], $_GET['id']) && $_GET['action'] == 'delete') {
    $id = $_GET['id'];
    User::delete($id);
}

if (isset($_GET['action'], $_GET['id']) && $_GET['action'] == 'edit') {
    $id = $_GET['id'];
    $currentUser = User::find($id);

}

if (isset(
    $_POST['full_name'],
    $_POST['username'],
    $_POST['email'],
    $_POST['password'],
    $_POST['re_password']
)) {

    // 1 step => validations
    $username = $_POST['username'];
    $fullName = $_POST['full_name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $rePassword = $_POST['re_password'];
    # regex

    // 2 step => new object from target class {User}
    $user = new User();

    // 3 step => set all required values
    $user->setFullName($fullName);
    $user->setUsername($username);
    $user->setEmail($email);
    $user->setPassword(md5($password));


    if (isset($_POST['id']) && empty($_POST['id'])) {
        // insert
        // 4 step => execute insert function
        $user->insert();
    } else {
        $id = $_POST['id'];
        $user->setId($id);

        // 4 step => execute update function
        $user->update();
    }



}
// get all users
$users = User::all();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">

    <br>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <form method="post">
                <input type="hidden" name="id"
                       value="<?php if (isset($currentUser)) echo $currentUser->id; ?>">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="full-name">Full Name</label>
                            <input type="text"
                                   class="form-control"
                                   id="full-name"
                                   name="full_name"
                                   value="<?php if (isset($currentUser)) echo $currentUser->full_name; ?>"
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text"
                                   class="form-control"
                                   id="username"
                                   name="username"
                                   value="<?php if (isset($currentUser)) echo $currentUser->username; ?>"
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email"
                                   class="form-control"
                                   id="email"
                                   name="email"
                                   value="<?php if (isset($currentUser)) echo $currentUser->email; ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password"
                                   class="form-control"
                                   id="password"
                                   name="password"
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="re-password">Re-Password</label>
                            <input type="password"
                                   class="form-control"
                                   id="re-password"
                                   name="re_password"
                            >
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Full Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user) {
            ?>
            <tr>
                <td><?php echo $user->id; ?></td>
                <td><?php echo $user->full_name; ?></td>
                <td><?php echo $user->username; ?></td>
                <td><?php echo $user->email; ?></td>
                <td>
                    <a class="btn btn-info"
                        href="?id=<?php echo $user->id; ?>&action=edit">
                        Edit
                    </a>
                    <a class="btn btn-danger"
                       href="?id=<?php echo $user->id; ?>&action=delete"
                        >
                        Delete
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>

</body>
</html>










