<?php
/**
 * Created by PhpStorm.
 * User: elnemr
 * Date: 6/22/2019
 * Time: 3:37 AM
 */

class User
{
    private $id;
    private $fullName;
    private $username;
    private $email;
    private $password;
    private static $dbh;

    public function __construct()
    {
        global $dbh;
        self::$dbh = $dbh;
    }

    private static function setDbh() {
        global $dbh;
        self::$dbh = $dbh;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    // insert
    public function insert()
    {
        $sql = "INSERT INTO `users` 
                (`username`, `full_name`, `email`, `password`) 
                VALUES(:username, :fullName, :email, :password) 
                ";
        $data = [
            'username' => $this->username,
            'fullName' => $this->fullName,
            'email' => $this->email,
            'password' => $this->password
        ];
        $stm = self::$dbh->prepare($sql);
        $stm->execute($data);

        return self::$dbh->lastInsertId();
    }

    // update
    public function update()
    {
        $sql = "UPDATE `users` SET 
                `full_name`=:fullName, 
                `username`=:username,
                `email`=:email,
                `password`=:password
                WHERE id=:id 
                ";
        $stm = self::$dbh->prepare($sql);
        $data = [
            'username' => $this->username,
            'fullName' => $this->fullName,
            'email' => $this->email,
            'password' => $this->password,
            'id' => $this->id
        ];

        return $stm->execute($data);
    }

    // find
    public static function find($id)
    {
        self::setDbh(); // get object from PDO

        $sql = "SELECT * FROM `users` WHERE id=:id";
        $stm = self::$dbh->prepare($sql);
        $data = ['id'=> $id];
        $stm->execute($data);

        return $stm->fetch(PDO::FETCH_OBJ);
    }

    // all
    public static function all()
    {
        self::setDbh();
        $sql = "SELECT * FROM `users`";
        $stm = self::$dbh->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_OBJ);
    }

    // delete
    public static function delete($id)
    {
        self::setDbh();
        $sql = "DELETE FROM `users` WHERE id=:id";
        $stm = self::$dbh->prepare($sql);
        $data = ['id'=> $id];
        return $stm->execute($data);
    }
}