<?php
$host = 'localhost';
$dbname = 'lms2';
$dsn = "mysql:host=$host;dbname=$dbname";
$dbuser = 'root';
$dbpassword = '';

try{
    $dbh = new PDO($dsn, $dbuser, $dbpassword);
} catch (Exception $e) {
    $error =  date('Y-m-d H:i:s') . "=>". $e->getMessage() . "\n";

    file_put_contents('dbErrors.txt', $error, FILE_APPEND);
}
