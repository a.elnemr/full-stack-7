<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('blog.blog', [
            'blogs'=> Blog::all()
        ]);
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(Request $request)
    {
        $blog = new Blog();
        $blog->title = $request['title'];
        $blog->body = $request['body'];
        $blog->user_id = \Auth::user()->id; // get current user id

        $blog->save();
        return redirect(route('blog.index'));
    }
}
