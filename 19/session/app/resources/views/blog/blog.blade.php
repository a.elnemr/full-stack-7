@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h1 class="card-header">List Blog</h1>
                    <ul>
                        @foreach($blogs as $blog)
                            <li>
                                <h3>{{ $blog->title }}</h3>
                                <p>{{ $blog->body }}</p>
                                <span>{{ $blog->user_id }}</span>
                                <span>{{ $blog->created_at }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </div>
@stop
