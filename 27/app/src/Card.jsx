import React from 'react';

export default function Card({student, editHandel}) {
    return (
        <div className="col-md-3">
            <div className="card">
                <div className="card-body">
                    <p className="card-text">
                        {student}
                        <hr/>
                        <button onClick={editHandel} className="btn btn-info">Edit</button>
                    </p>
                </div>
            </div>
        </div>
    )
}