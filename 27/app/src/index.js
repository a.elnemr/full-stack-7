import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';


// const app = <h1>React App 55</h1>;

// function App() {
//     return (
//         <h1>React App 55</h1>
//     );
// }


ReactDOM.render(
    <App />
    ,document.getElementById('root'));
