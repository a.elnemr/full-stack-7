import React, {Fragment, Component} from 'react';
import Card from "./Card";

class App extends Component {

    state = {
        name: 'Demo',
        students: [
            'Hazem',
            'Ahmed',
            'Hesham',
            'Ali'
        ]
    };

    editClickHandle = () => {
      alert('clicked');
    };

    // gtAllStudent = editClick => (
    //     this.state.students.map(function (student, index) {
    //         return (<Card key={index}
    //                       student={student}
    //                       index={index}
    //                       editHandel={editClick}/>);
    //     })
    // );


    render() {
        return (
            <Fragment>
                <div className="container">
                    <button onClick={this.editClickHandle}>test</button>
                    <h1>{this.state.name}</h1>
                    <div className="row mt-2">
                        {/*{this.gtAllStudent(()=>this.editClickHandle())}*/}

                        {this.state.students.map((student, index)=> (
                                <Card key={index}
                                      student={student}
                                      editHandel={()=> this.editClickHandle()} />
                            )
                        )}


                    </div>
                </div>
            </Fragment>
        );
    }
}

export default App;