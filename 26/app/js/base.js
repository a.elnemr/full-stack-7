// $(document).ready(function () {
//     alert('test');
// });

$(function () {
    $('#all-posts').click(function () {
        $('.content').html('');
        getDataFromApi();
    });
});

$(document).on('click', '.show-post', function () {
    const postId = $(this).attr('post-id');
    $.ajax({
        type: 'GET',
        url: `https://jsonplaceholder.typicode.com/posts/${postId}`,
        success: function (res) {
            $('.content').html(`
                    <article>
                        <h3>${res.title}</h3>
                        <p>${res.body}</p>
                        <p>${res.userId}</p>
                        <hr>
                    </article>
            `);
        }
    });
});

function printPosts(post) {
    $('.content').append(`<article>
                    <h3>
                        <a href="#" 
                            class="show-post" 
                            post-id="${post.id}">
                            ${post.title}
                        </a>
                    </h3>
                    
                    <hr>
                </article>`);
}

function getDataFromApi() {
    $.ajax({
        type: 'GET',
        url: 'https://jsonplaceholder.typicode.com/posts',
        success: function (res) {
            for (let i=0; i<res.length; i++) {
                printPosts(res[i]);
            }
        }
    });
}