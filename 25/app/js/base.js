$('#menu').click(function () {
    // $('ul').fadeToggle(500);
    $('ul p').toggle(200);
    $('ul span').toggle(200);
    $('ul').toggleClass('w-50', 'w-100');
    // $('.model').fadeIn(500);
});

$('.model span').click(function () {
    $(this).parent().fadeOut(500);
});


$('button.open-model').click(function () {
    const modelName = $(this).attr('model-name');
    $('#'+modelName).fadeIn(500);
});
