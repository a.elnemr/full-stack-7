<?php
$students = [
    [
        'name'=> 'Ahmed',
        'age'=> 25,
        'color'=> 'red',
        'subj'=> ['php', 'html', 'sql']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ],
];

//print_r($students);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Age</th>
        <th>Color</th>
        <th>Subjects</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($students as $id=> $student) {
        ?>
        <tr>
            <td><?php echo $id; ?></td>
            <td><?php echo $student['name']; ?></td>
            <td><?php echo $student['age']; ?></td>
            <td><?php echo $student['color']; ?></td>
            <td>
                    <?php
                    foreach ($student['subj'] as $item) {
                        echo $item . ' | ';
                    }
                    ?>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>

</table>
</body>
</html>
