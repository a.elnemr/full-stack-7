<?php
//$name = 'Ahmed';
//
//$students = array(array(1, 2, 3), 1*5, false, 'ali', $name);
//print_r($students);
//
//$students[] = 'new name';

//print_r($students);
//var_dump($students);
//echo $students[count($students) - 1];
//$students = array(
//    array('Ahmed', 25),
//    array('Ali', 30)
//);
$students = [
    [
        'name'=> 'Ahmed',
        'age'=> 25,
        'color'=> 'red',
        'subj'=> ['php', 'html']
    ],
    [
        'name'=> 'Ali',
        'age'=> 30,
        'color'=> 'green',
        'subj'=> ['css', 'js']
    ]
];