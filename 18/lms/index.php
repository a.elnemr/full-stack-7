<?php
require_once 'config/const.php';
require_once CONFIG_DIR .'dbConfig.php';
require_once MODEL_DIR .'Model.php';
require_once CONFIG_DIR .'autoload.php';


$page = isset($_GET['page'])? $_GET['page'] : 'home';

require_once 'layout/header.tpl';
switch ($page) {
    case 'home':
        require_once 'view/home.php';
        break;
    case 'users':
        require_once 'view/users.php';
        break;
    default:
        require_once 'view/404.php';
}

require_once 'layout/footer.tpl';