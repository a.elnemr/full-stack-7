<?php
try {
    $dbh = new PDO($dsn, $username, $password);
} catch (PDOException $e) {
    $message = date('Y-m-d H:i:s') . ' => ' . $e->getMessage() . "\n";
    file_put_contents('db-errors.txt', $message, FILE_APPEND);
    die('Website down');
}