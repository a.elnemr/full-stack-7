<?php

// database connection
$host = 'localhost';
$dbName = 'lms';
$dsn = "mysql:host=$host;dbname=$dbName";
$username = 'root';
$password = '';

//
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_DIR', dirname(__DIR__) . DS);
define('MODEL_DIR', ROOT_DIR . 'model' . DS);
define('VIEW_DIR', ROOT_DIR . 'view' . DS);
define('CONFIG_DIR', ROOT_DIR . 'config' . DS);