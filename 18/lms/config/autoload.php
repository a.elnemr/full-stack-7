<?php
$models = scandir(MODEL_DIR , 1);

foreach ($models as $model) {
    if ($model == '.' || $model == '..') {
        continue;
    }
    require_once MODEL_DIR . $model;
}