-- DDL
DROP DATABASE lms;
CREATE DATABASE lms;

USE lms;

CREATE TABLE users(
  id INT PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(60) UNIQUE,
  phone CHAR(11),
  username VARCHAR(20) NOT NULL UNIQUE,
  password VARCHAR(80) NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE teachers(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(15) NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE students(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(15) NOT NULL,
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);

CREATE TABLE courses(
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(30) NOT NULL,
  duration FLOAT,
  description TEXT,
  teacher_id INT
);