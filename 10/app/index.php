<html>
    <head>
        <title>Session 10</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1>Registration Form</h1>
                    <form class="form" action="register.php" method="get">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="name">Name: </label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" id="name" name="name">
                            </div>


                        </div>
                        <div class="form-group">
                            <label for="username">Username: </label>
                            <input type="text" id="username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="email">Email: </label>
                            <input type="email" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="password">Password: </label>
                            <input type="password" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="re-password">Re-Password: </label>
                            <input type="password" id="re-password" name="re_password">
                        </div>
                        <div class="form-group">
                            <label for="re-password">Gender: </label>
                            Male: <input type="radio" name="gender" value="MALE">
                            Female: <input type="radio" name="gender" value="FEMALE">
                        </div>
                        <div class="text-center">
                            <button class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>
</html>
