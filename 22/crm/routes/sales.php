<?php
/**
 * Created by PhpStorm.
 * User: elnemr
 * Date: 7/13/2019
 * Time: 8:56 AM
 */

// Sales routes
Route::group(['middleware' => ['auth', 'Sales']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
//    Route::resource('/calls', 'CallController'); // @TODO updated ACL

    Route::get('/calls/create/{lead}', 'CallController@create')
        ->name('calls.create');
    Route::post('/call/{lead}', 'CallController@store')
        ->name('calls.store');
});
