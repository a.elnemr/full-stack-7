<?php
/**
 * Created by PhpStorm.
 * User: elnemr
 * Date: 7/13/2019
 * Time: 8:55 AM
 */

// Team leader routes
Route::group(['middleware' => ['auth', 'TeamLeader']], function () {
    Route::get('/leads', 'LeadController@index')->name('leads.index');
    Route::get('/leads/{lead}', 'LeadController@show')->name('leads.show');
    Route::post('/leads/assigned', 'LeadController@assigned')->name('leads.assigned');
//    Route::resource('/calls', 'CallController');

    Route::get('/calls/create/{lead}', 'CallController@create')
        ->name('calls.create');
    Route::post('/call/{lead}', 'CallController@store')
        ->name('calls.store');
});
