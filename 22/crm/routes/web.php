<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

require_once 'admin.php';
require_once 'teamLeader.php';
require_once 'sales.php';


// Errors
Route::get('/403', function () {
    return view('errors.403');
})->name('403');

Route::get('/get-sales/{lead}', 'LeadController@updateSalesByLeadId');
