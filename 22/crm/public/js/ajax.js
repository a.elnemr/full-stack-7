$(document).on('click', '.update-sales', function () {
   const leadId = $(this).attr('lead-id');
   $.ajax({
       type: 'GET',
       url: '/get-sales/'+leadId,
       success: function (res) {
           $('#modal-assigned .modal-body').html(res);
       }
   })
});
