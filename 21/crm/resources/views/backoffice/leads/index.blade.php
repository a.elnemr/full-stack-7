@extends('layouts.app')

@section('title', 'Leads')

@section('pageHeader', 'All Leads')

@section('content')
    <div class="text-right p-3">
        <a href="{{ route('leads.create') }}" class="btn btn-success">ADD NEW</a>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leads as $lead)
        <tr>
            <td>{{ $lead->id }}</td>
            <td>{{ $lead->name }}</td>
            <td>{{ $lead->phone }}</td>
            <td>{{ $lead->email }}</td>
            <td>
                <a href="{{ route('leads.show', $lead) }}" class="btn btn-info">Show</a>
                <a href="{{ route('leads.edit', $lead) }}" class="btn btn-primary">Edit</a>
                <form action="{{ route('leads.destroy', $lead) }}" method="post" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
