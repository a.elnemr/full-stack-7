import React, { Component } from 'react';
import FormPost from "./FormPost";
import axios from "axios";

class CreatePost extends Component{

    state = {
        post: {
            title: '',
            body: ''
        }
    };

    //
    // onChangeTitle = (e) => {
    //   const inputValue = e.target.value;
    //   let post = this.state.post;
    //   post.title = inputValue;
    //   this.setState({post});
    // };
    //
    // onChangeBody = (e) => {
    //   const inputValue = e.target.value;
    //   let post = this.state.post;
    //   post.body = inputValue;
    //   this.setState({post});
    // };

    changeHandle = e => {
      const inputValue = e.target.value;
      let post = this.state.post;
      post[e.target.id] = inputValue;
      this.setState({post});
    };

    submitHandle = e => {
        e.preventDefault();
        const {post} = this.state;
        const url = `https://jsonplaceholder.typicode.com/posts`;
        axios.post(url, {post}).then(response => response.data)
            .then((data) => {
                this.setState({post: data});
                console.log(data);
            })
    };

    render() {
        return (
            <div>
                <FormPost post={this.state.post}
                          onSubmit={(e)=> this.submitHandle(e)}
                          onChange={(e)=> this.changeHandle(e)}
                />
            </div>
        );
    }

}


export default CreatePost;