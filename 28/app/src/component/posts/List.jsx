import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import axios from "axios";

class ListPosts extends Component {
    state = {
        posts: []
    };

    componentDidMount() {
        const url = `https://jsonplaceholder.typicode.com/posts`;
        axios.get(url).then(response => response.data)
            .then((data) => {
                this.setState({posts: data});
            })
    }

    render() {
        return (
            <table className="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>Title</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {this.state.posts.map(
                    post => (
                        <tr key={post.id}>
                            <td>{post.id}</td>
                            <td>{post.userId}</td>
                            <td>
                                <NavLink to={'/post/' + post.id}>{post.title}</NavLink>
                            </td>
                            <td>
                                <NavLink to={'/post/' + post.id + '/edit'} className="btn btn-info">
                                    Edit
                                </NavLink>
                            </td>
                        </tr>
                    )
                )}
                </tbody>
            </table>
        )
    }
}

export default ListPosts;