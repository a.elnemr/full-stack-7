import React, {Component} from "react";
import FormPost from "./FormPost";
import axios from "axios";

class EditPost extends Component{

    state = {
        post: {}
    };

    changeHandle = e => {
        const inputValue = e.target.value;
        let post = this.state.post;
        post[e.target.id] = inputValue;
        this.setState({post});
    };

    submitHandle = e => {
        e.preventDefault();
        const {id} = this.props.match.params;
        const {post} = this.state;
        const url = `https://jsonplaceholder.typicode.com/posts/${id}`;
        axios.put(url, {post}).then(response => response.data)
            .then((data) => {
                this.setState({post: data});
                console.log(data);
            })
    };

    componentDidMount() {
        const {id} = this.props.match.params;
        const url = `https://jsonplaceholder.typicode.com/posts/${id}`;
        axios.get(url).then(response => response.data)
            .then((data) => {
                this.setState({post: data});
            })
    }

    render() {
        return (
            <div>
                <FormPost post={this.state.post}
                          onChange={ e =>this.changeHandle(e) }
                          onSubmit={ e =>this.submitHandle(e) }

                />
            </div>
        );
    }

}

export default EditPost;