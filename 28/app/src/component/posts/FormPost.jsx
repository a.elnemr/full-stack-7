import React from "react";

export default function FormPost({post, onChange, onSubmit}) {
    return (
        <form className="form" onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input type="text"
                       id="title"
                       className="form-control"
                       value={post.title}
                       onChange={onChange}
                />
            </div>
            <div className="form-group">
                <label htmlFor="body">Body</label>
                <textarea id="body"
                          cols="30"
                          rows="10"
                          className="form-control"
                          value={post.body}
                          onChange={onChange}
                />
            </div>
            <button type="submit" className="btn btn-success">Save</button>
        </form>
    );
}