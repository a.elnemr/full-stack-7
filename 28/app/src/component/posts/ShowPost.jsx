import React, {Component, Fragment} from 'react';
import axios from "axios";

export default class ShowPost extends React.Component{
    state = {
        post: {}
    };

    componentDidMount() {
        const {id} = this.props.match.params;
        const url = `https://jsonplaceholder.typicode.com/posts/${id}`;
        axios.get(url).then(response => response.data)
            .then((data) => {
                this.setState({post: data});
            })
    }

    render() {
        return (
            <div>
                <h3><strong>Title:</strong> {this.state.post.title}</h3>
                <p><strong>Body:</strong> <br /> {this.state.post.body}</p>
            </div>
        );
    }

}