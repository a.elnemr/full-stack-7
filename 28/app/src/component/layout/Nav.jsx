import React from 'react';
import {Link, NavLink} from 'react-router-dom';

function Nav() {
    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav">
                        <NavLink to="/" exact className="nav-item nav-link">Home</NavLink>
                        <NavLink to="/create" className="nav-item nav-link">New Post</NavLink>
                    </div>
                </div>
            </nav>
            <hr />
        </React.Fragment>
    );
}

export default Nav;
