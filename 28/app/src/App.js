import React, { Component } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import Nav from "./component/layout/Nav";
import ListPosts from "./component/posts/List";
import ShowPost from "./component/posts/ShowPost";
import CreatePost from "./component/posts/CreatePost";
import EditPost from "./component/posts/EditPost";

class App extends Component{

    render() {
        return (
            <div className="container">
                <Router>
                    <Nav/>
                    <Route exact path="/" component={ListPosts} />
                    <Route exact path="/create" component={CreatePost} />
                    <Route exact path="/post/:id" component={ShowPost} />
                    <Route exact path="/post/:id/edit" component={EditPost} />
                </Router>
            </div>
        )
    }
}

export default App;
