<?php
function displayData($data) {
    ?>
    <ul>
        <?php
        foreach ($data as $item) {
            ?>
            <li><?php echo $item['name']; ?></li>
        <?php
        }// end of loop
        ?>
    </ul>

<?php
} // end of function

function insertData($data, $item) {
    $newData = $data;
    $newData[] = $item;
    return $newData;
}

function updateData($data, $key, $newValue) {
    $data[$key]['name'] = $newValue;
    return $data;
}

function searchById($data, $id) {
    foreach ($data as $key=> $row) {
        if ($row['id'] == $id){
            return $key;
        }
    }
}

function deleteById($data, $id) {
    unset($data[$id]);
    return $data;
}