<?php
require_once 'data.php';
require_once 'functions.php';

$newData1 = insertData($data, ['name'=> 'new row', 'id'=> 7]);
$targetIndex = searchById($data, '22');
$updatedData1 = updateData($data2, $targetIndex, 'Ali 50');
$deletedData = deleteById($data, searchById($data, 1));

displayData($newData1);
displayData($updatedData1);
displayData($deletedData);
