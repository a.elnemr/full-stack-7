@extends('layouts.app')

@section('title', 'Todo')

@section('content')
<h2>Todo List</h2>
<a href="{{ route('todo.create') }}" class="btn btn-success">New</a>
<ul>
   @foreach($todos as $todo)
       <li class="p-3">
           <h4>{{ $todo->title }}</h4>
           <h5>{{ $todo->user->name }}</h5>
           <span>{{ $todo->created_at }}</span>
           <a href="{{ route('todo.show', $todo->id) }}" class="btn btn-info">Show</a>
           <a href="{{ route('todo.edit', $todo->id) }}" class="btn btn-primary">Edit</a>
           <form action="{{ route('todo.destroy', $todo) }}" method="post">
               @csrf
               @method('DELETE')
               <button type="submit" class="btn btn-danger">Delete</button>

           </form>
       </li>
   @endforeach
</ul>
@stop
