@extends('layouts.app')

@section('title', 'Todo Show')

@section('content')
    <h4>{{ $todo->title }}</h4>
    <h5>{{ $todo->user->name }}</h5>
    <span>{{ $todo->created_at }}</span>
    <p>{{ $todo->description }}</p>
    <a href="{{ route('todo.edit', $todo->id) }}" class="btn btn-primary">Edit</a>
@stop
