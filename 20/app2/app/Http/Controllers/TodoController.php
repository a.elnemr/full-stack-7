<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoRequest;
use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 1st way to send data to view
//        return view('todo.index')->with('todos', Todo::all());

        // 2nd way to send data to view
//        $todos = Todo::all();
//        return view('todo.index', compact('todos')));

        // 3rd way to send data to view
//        return view('todo.index', array('todos'=> Todo::all()));

        return view('todo.index', ['todos'=> Todo::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
//        $request->validate([
//            'title' => 'required|max:191'
//        ]);

        $todo = new Todo();
        $todo->title = $request['title'];
        $todo->description = $request['description'];
        $todo->user_id = Auth::user()->id;

        $todo->save();
        return redirect()->route('todo.show', $todo->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        return view('todo.show', ['todo'=> $todo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        return view('todo.edit', ['todo'=> $todo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request, Todo $todo)
    {
//        $request->validate([
//            'title' => 'required|max:191'
//        ]);

        $todo->title = $request->title;
        $todo->description = $request->description;
        $todo->save();
        return redirect()->route('todo.show', $todo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        $todo->delete();
        return redirect()->route('todo.index');
    }
}
