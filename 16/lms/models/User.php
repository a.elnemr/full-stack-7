<?php

class User {
    private static $dbh;
    private $id;
    private $username;
    private $email;
    private $phone;
    private $password;


    public function getId()
    {
        return $this->id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function __construct()
    {
        global $dbh;
        $this->dbh = $dbh;
    }

    private static function setDbh() {
        global $dbh;
        self::$dbh = $dbh;
    }

    public function insert()
    {
        $sql = "INSERT INTO `users` (`email`, `phone`, `username`, `password`) 
                VALUES (:email, :phone, :username, :password)";

        $stm = $this->dbh->prepare($sql);
        $data = [
            'email' => $this->email,
            'phone' => $this->phone,
            'username' => $this->username,
            'password' => $this->password
        ];

        $stm->execute($data);

        return $this->dbh->lastInsertId();
    }

    public static function all()
    {
        self::setDbh();
        $sql = 'SELECT * FROM `users`';

        $stm = self::$dbh->prepare($sql);
        $stm->execute();

        return $stm->fetchAll(PDO::FETCH_OBJ);
    }

    public static function find($id) {
        self::setDbh();

        $sql = 'SELECT * FROM `users` WHERE id = :id';

        $stm = self::$dbh->prepare($sql);
        $data = [
            'id' => $id
        ];
        $stm->execute($data);

        return $stm->fetch(PDO::FETCH_OBJ);
    }


    public function update()
    {

    }

}