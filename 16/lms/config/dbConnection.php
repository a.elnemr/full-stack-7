<?php
$dbName = 'lms';
$host = 'localhost';
$username = 'root';
$password = '';

try{
    $dbh = new PDO("mysql:host=$host;dbname=$dbName", $username, $password);
} catch (Exception $e) {
    file_put_contents('pdo-errors.txt', date('Y-m-d H:i:s') . ' => ' . $e->getMessage() . "\n", FILE_APPEND);
}