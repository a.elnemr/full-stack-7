<?php
class Student {

    public $id;
    public $name;
    public $age;


    public function add() {
        echo 'ADD';
    }

    public function delete() {
        echo 'DELETE';
    }
}

$obj1 = new Student;
$obj2 = new Student;


$obj1->id = 1;
$obj1->name = "Mohamed";
$obj1->age = 19;

$obj2->id = 2;
$obj2->name = 'Ahmed';
$obj2->age = 20;

$arr3 = array(
    $obj1,
    $obj2,
    new Student()
);


print_r($arr3);
